defmodule Ecs.EntityRegistryTest do
  use ExUnit.Case
  doctest Ecs.EntityRegistry

  alias Ecs.Component
  alias Ecs.Entity
  alias Ecs.EntityRegistry

  test "can query entities by component type" do
    robot_entity = robot_entity()
    human_entity = human_entity()

    {:ok, registry_pid} = Ecs.EntityRegistry.start_link([])
    EntityRegistry.add_entity(registry_pid, robot_entity)
    EntityRegistry.add_entity(registry_pid, human_entity)

    renderable_entities = EntityRegistry.query_by_component_type(registry_pid, :renderable)
    assert renderable_entities == [robot_entity, human_entity]
  end

  test "can query entities by aspect" do
    robot_entity = robot_entity()
    human_entity_a = human_entity()
    human_entity_b = human_entity()

    {:ok, registry_pid} = Ecs.EntityRegistry.start_link([])
    EntityRegistry.add_entity(registry_pid, robot_entity)
    EntityRegistry.add_entity(registry_pid, human_entity_a)
    EntityRegistry.add_entity(registry_pid, human_entity_b)

    human_entities = EntityRegistry.query_by_aspect(registry_pid, [:physical, :organic, :social])
    assert human_entities == [human_entity_a, human_entity_b]
  end

  test "can remove entity from component type indexes" do
    robot_entity = robot_entity()
    human_entity = human_entity()

    {:ok, registry_pid} = Ecs.EntityRegistry.start_link([])
    EntityRegistry.add_entity(registry_pid, robot_entity)
    EntityRegistry.add_entity(registry_pid, human_entity)

    EntityRegistry.delete_entity(registry_pid, robot_entity)

    entities = EntityRegistry.query_by_component_type(registry_pid, :renderable)
    assert entities == [human_entity]
  end

  test "monitors and removes entity processes from the registry" do
    robot_entity = robot_entity()
    human_entity = human_entity()

    {:ok, registry_pid} = Ecs.EntityRegistry.start_link([])
    EntityRegistry.add_entity(registry_pid, robot_entity)
    EntityRegistry.add_entity(registry_pid, human_entity)

    Agent.stop(robot_entity.pid, :kill)

    entities = EntityRegistry.query_by_component_type(registry_pid, :renderable)
    assert entities == [human_entity]
  end

  defp robot_entity do
    Entity.new([
      Component.new(:renderable),
      Component.new(:physical),
      Component.new(:metallic)
    ])
  end

  defp human_entity do
    Entity.new([
      Component.new(:renderable),
      Component.new(:physical),
      Component.new(:organic),
      Component.new(:social)
    ])
  end
end
