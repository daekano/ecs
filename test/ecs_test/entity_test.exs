defmodule Ecs.EntityTest do
  use ExUnit.Case
  doctest Ecs.Entity

  alias Ecs.Component
  alias Ecs.Entity

  test "can create an entity" do
    %{id: id, pid: pid} = Entity.new()

    assert id != nil
    assert pid != nil
  end

  test "can create an entity with components and retrieve them" do
    components = [
      Component.new(:renderable),
      Component.new(:physical)
    ]

    entity = Entity.new(components)
    entity_components = Entity.get_components(entity)

    assert entity_components == components
  end
end
