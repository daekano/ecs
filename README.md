# Entity-Component System for Elixir

## Entity

Entities are a logical identifier in the ECS, which Components can be attached to in order to impart behavior and other important properties. They themselves do not hold data other than the information necessary to differentiate between different Entities.

Our Entities will be identified by two different values:

1. ID: A string UUID value that is used by the application to differentiate between different Entities.
2. PID: A reference to an Erlang Process which contains and manages Component state for the Entity.

An Entity struct would look like the following:

```elixir
%Ecs.Entity{id: 'dd2822f0-4565-11ea-a695-9199260a230e', pid: "<0.39.0>"}
```

## Component

Components are discrete blocks of data that are 'attached' to Entities. Like Entities, they have a UUID, but their primary properties are the type and any associated (but optional) data.

```elixir
%Ecs.Component{id: 'dd2822f0-4565-11ea-a695-9199260a230e', type: :renderable}
```

We want our Systems to be able to query for Components in a few different ways:

1. Entity queries and filters:
   - This method will be useful when needing to operate on components that are attached to Entities that match certain criteria, such as a location-based query.
   - `select all Entities within polygon A`
2. Component type:
   - Retrieve entities by a single Component type.
   - `select all HealthComponents`
3. Aspect:
   - This method is a set of Component types but given special semantics by the querying System.
   - `select all HealthComponents and SocialComponents as CharacterAspect`

## System

Systems operate on Component data, executing continuously with or without user input.

They will each be expressed using a persistent process (GenServer or Agent), and may communicate laterally using events.

Systems will integrate with the Entity Registry in order to find Entities that are relevant to its responsibilities, by issuing queries as described in the Component section. Systems will also be able to encode an "Aspect", which is a query comprised of multiple Component Types that are semantically appropriate to the System.

## Entity Registry

This is a huge central part of the entire operation. Being able to store, query, access, and change Entities and their associated Components in a performant way is crucial to the scalability of the ECS. ETS querying is impenetrable for us at this point. Designing and using a domain-specific Entity Registry also allows us to express our domain as we see fit and hide implmentation details should we ever wish to change it out in the future.

The idea is to build a data structure that references entities in a flat list by the Component name.

```elixir
entities = [entity1, entity2, entity3]

registry = %{
  renderable: [entity1, entity2, entity3],
  physical: [entity1, entity3]
}
```

The Registry module would monitor the Entity processes and remove references should they be deleted or exit, and would add/remove index references should the Entity's makeup of Components change.
