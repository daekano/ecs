defmodule Ecs.Entity do
  @moduledoc """
  Entities are logical nodes within the whole system. We can attach Components to them in order to impart behaviour.
  We represent Entities with Entity IDs, and are trying to keep Process IDs internal to the Entity implementation.
  The Process ID is used to access the Entity's state through the Entity module's API.
  """
  @enforce_keys [:id, :pid]
  defstruct [:id, :pid]

  @doc """
  Create an Entity.
  This operation is twofold:
      - Start a Process that contains the Entity's Components
      - Create a UUID to discriminate between Entities without resorting to the PID
  Having two identifiers means we can discriminate in a domain-specific way and also hide the fact that we use a process to manage state.
  """
  def new(components \\ [])

  def new(components) when is_list(components) do
    {:ok, pid} = Agent.start(fn -> MapSet.new(components) end)
    %Ecs.Entity{id: UUID.uuid1(), pid: pid}
  end

  @doc """
  Queries an Entity process for its components, transforming the output from a MapSet to a List.
  The Agent process stores Components in a MapSet for data integrity reasons.
  """
  def get_components(%Ecs.Entity{} = entity) do
    Agent.get(entity.pid, fn components -> MapSet.to_list(components) end)
  end
end
