defmodule Ecs.EntityRegistry do
  @moduledoc """
  The EntityRegistry manages the lifecycle of Entities and their Components.
  An Entity is added to the EntityRegistry when it is created.
  Entity Processes are removed from the EntityRegistry's index when they exit.
  Entity references in the index are changed when an Entity's Component makeup changes.

  Systems will use the EntityRegistry to query for Entities a number of different ways:
      - By Component State, e.g., All entities by position, zone, or an arbitrary state value
      - By Component, e.g., all by Components of types [:renderable] or [:renderable, :social]
  """

  use GenServer

  alias Ecs.Entity
  alias Ecs.EntityRegistry

  @enforce_keys [:entities, :indexes]
  defstruct entities: MapSet.new(), indexes: %{}, monitors: %{}

  @doc """
  Start a new EntityRegistry GenServer process.
  It accepts a struct of the same module as the state.
  """
  def start_link(options) do
    GenServer.start_link(
      __MODULE__,
      EntityRegistry.new(),
      options
    )
  end

  @impl true
  def init(state) do
    {:ok, state}
  end

  # Client

  @doc """
  Return an initialised EntityRegistry struct
  """
  def new, do: %EntityRegistry{entities: MapSet.new(), indexes: %{}}

  @doc """
  Add an entity to the registry.
  """
  def add_entity(pid, %Ecs.Entity{} = entity) do
    GenServer.call(pid, {:add_entity, entity})
  end

  @doc """
  Remove an entity from the registry.
  """
  def delete_entity(pid, %Ecs.Entity{} = entity) do
    GenServer.call(pid, {:delete_entity, entity})
  end

  @doc """
  Fetch all Entities that match the given component type.
  """
  def query_by_component_type(pid, component_type) when is_atom(component_type) do
    GenServer.call(pid, {:query_by_component_type, component_type})
  end

  @doc """
  Fetch all Entities that match the given aspect.
  An aspect is a composite list of components.
  Any returned entities must implement the full aspect.
  """
  def query_by_aspect(pid, aspect) when is_list(aspect) do
    GenServer.call(pid, {:query_by_aspect, aspect})
  end

  # Server

  @doc """
  Add Entity
  GenServer Handler
  """
  @impl true
  def handle_call({:add_entity, entity}, _from, state) do
    # Watch the entity's pid for any failures so we can remove it from the indexes
    ref = Process.monitor(entity.pid)

    state =
      state
      # Add entity to the entities MapSet
      |> Map.put(:entities, MapSet.put(state.entities, entity))
      # Add entity references to the component type indexes
      |> Map.put(:indexes, insert_entity_indexes(state.indexes, entity))
      # Add monitor reference so we can look up our entity if it dies
      |> Map.put(:monitors, Map.put(state.monitors, ref, entity))

    {:reply, :ok, state}
  end

  @doc """
  Delete Entity
  GenServer Handler
  """
  @impl true
  def handle_call({:delete_entity, entity}, _from, state) do
    state = Map.put(state, :indexes, delete_entity_indexes(state.indexes, entity))

    {:reply, :ok, state}
  end

  @doc """
  Query By Component Type
  GenServer Handler
  """
  @impl true
  def handle_call({:query_by_component_type, component_type}, _from, state) do
    entities =
      state.indexes
      |> Map.get(component_type)
      |> MapSet.to_list()

    {:reply, entities, state}
  end

  @doc """
  Query By Aspect
  GenServer Handler
  """
  @impl true
  def handle_call({:query_by_aspect, aspect}, _from, state) do
    entities =
      state.indexes
      # Narrow down to only the indexes for component keys we have
      |> Map.take(aspect)
      # Convert the map to a list, omitting the keys
      |> Enum.map(fn {_key, index} -> index end)
      # Compare each index in the list, omitting entities that don't match
      |> Enum.reduce(fn index, acc -> MapSet.intersection(index, acc) end)
      |> MapSet.to_list()

    {:reply, entities, state}
  end

  @doc """
  Handle Monitored Processes
  GenServer Handler
  """
  @impl true
  def handle_info({:DOWN, ref, :process, _pid, _reason}, state) do
    entity = Map.get(state.monitors, ref)

    state =
      state
      # Remove the entity from the entities
      |> Map.put(:entities, MapSet.delete(state.entities, entity))
      # Remove the entity from the indexes
      |> Map.put(:indexes, delete_entity_indexes(state.indexes, entity))
      # Remove the entity in the monitor by reference
      |> Map.put(:monitors, Map.delete(state.monitors, ref))

    {:noreply, state}
  end

  defp insert_entity_indexes(indexes, %Entity{} = entity) when is_map(indexes) do
    entity
    |> Entity.get_components()
    |> Enum.reduce(indexes, fn component, acc ->
      Map.update(acc, component.type, MapSet.new([entity]), fn index ->
        MapSet.put(index, entity)
      end)
    end)
  end

  defp delete_entity_indexes(indexes, %Entity{} = entity) when is_map(indexes) do
    indexes
    |> Enum.map(fn {key, value} -> {key, MapSet.delete(value, entity)} end)
    |> Enum.into(%{})
  end
end
