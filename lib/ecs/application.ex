defmodule Ecs.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @doc """
  Start the application.
  Define all OTP supervisors and workers that should be started in the root supervisor tree.
  """
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: Ecs.Worker.start_link(arg)
      # {Ecs.Worker, arg}
      {Ecs.EntityRegistry, []}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Ecs.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
