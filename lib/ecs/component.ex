defmodule Ecs.Component do
  @moduledoc """
  Components hold all data and must be associated to Entities to have any meaning or value.
  Each Component has an ID and a type, as well as any additional state necessary to fulfill its role.
  A Component's state is essentially optional; a Component can be used simply to tag an entity to be of a certain type.
  e.g., a :renderable component may only need to indicate that an Entity should be rendered by the rendering system.
  """
  @enforce_keys [:id, :type]
  defstruct [:id, :type, state: %{}]

  @doc """
  Create a Component.
  Requires a type (atom) and an optional state (map)
  """
  def new(type, state \\ %{}) when is_atom(type) and is_map(state) do
    %Ecs.Component{id: UUID.uuid1(), type: type, state: state}
  end
end
